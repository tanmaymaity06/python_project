#####python project

#######################
######coining flipping program:
import random
def coinToss():
    number = int(input("Number of times to flip coin: "))
    heads_counter = 0
    tails_counter = 0
    for amount in range(number):
         flip = random.randint(0, 1)
         if (flip == 0):
             heads_counter+=1 
             print("Heads")
         else:
             tails_counter+=1 
             print("Tails")
    print("the no. of times the coin was flipped was:",number,"times")              
    print("the number of times tails:",tails_counter,"Times") 
    print("the number of times heads:",heads_counter,"Times") 
coinToss()


############################
####company manager####
class Company:
    def __init__(self,company_name):
        self.company_name=company_name
        
        # Create a dictionary of employee types, with lists to hold multiple employees
        self.type_of_emp = {'H':[],'S':[],'M':[],'E':[]}
    
    #defining various type of emplyee to hire
    def hire_hourly_emp(self,eid,name,project_name):
        rate_per_hour=400
        hours=4
        netsal=HourlyEmployee.pay(self,rate_per_hour,hours)
        self.type_of_emp['H'].append(HourlyEmployee(eid,name,project_name,netsal))
        print("hourly emp added")
    def hire_salaried_emp(self,eid,name,post,pf_percent):
        wage=850
        netsal=SalariedEmployee.pay(self,wage,pf_percent)
        self.type_of_emp['S'].append(SalariedEmployee(eid,name,post,pf_percent,netsal))
        print("salaried emp hired")
    def hire_manager_emp(self,eid,name,post,pf_percent):
        wage=2000
        netsal=Manager.pay(self,wage,pf_percent)
        self.type_of_emp['M'].append(Manager(eid,name,post,pf_percent,netsal))
        print("manager hired")
    def hire_executive_emp(self,eid,name,post,pf_percent):
        wage=5000
        netsal=Executive.pay(self,wage,pf_percent)
        self.type_of_emp['E'].append(Executive(eid,name,post,pf_percent,netsal))
        print("executive hired")

    #firing a employee
    def __del__(self):
        print("emp fired!")

#to fetch payment detail of any type of employee:
def get_pay(name,eid,emp_type):
    for name in name.type_of_emp[emp_type]:
        if name.eid==eid:
            print(name.netsal)
#to calculate the raise which has to be given:
def give_raise(name,eid,emp_type,percent_raise):
    for name in name.type_of_emp[emp_type]:
        if name.eid==eid:
            name.netsal=name.netsal+name.netsal*(percent_raise*0.01)
            print(name.netsal)
            
#abstract employee class
class Employee():
    def __init__(self,eid,name):
        self.eid=eid
        self.name=name
    def pay(self):
        raise NotImplementedError()
        
#different types of employees inheriting employee class
class HourlyEmployee(Employee):
    def __init__(self,eid,name,project_name):
        super().__init__(eid,name)
        self.project_name=project_name
    def __str__(self):
        pass
    def pay(self,rate_per_hour,hours):
        netsalary=HourlyEmployee.hours*HourlyEmployee.rate_per_hour
        print(netsalary)
        return netsalary
class SalariedEmployee(Employee):
    def __init__(self,eid,name,post,pf_percent,netsal):
        super().__init__(eid,name)
        self.post=post
        self.pf_percent=pf_percent
        self.netsal=netsal
    def __str__(self):
        pass
#calculate the payment of salaried employee
    def pay(self,wage,pf_percent):
        basic=wage*30
        hra=basic*0.2
        da=basic*0.1
        pf=basic*(0.01*pf_percent)
        netsalary=basic+hra+da-pf
        print(netsalary)
        return netsalary
class Manager(Employee):
    def __init__(self,eid,name,post,pf_percent,netsal):
        super().__init__(eid,name)
        self.post=post
        self.pf_percent=pf_percent
        self.netsal=netsal
    def __str__(self):
        pass
#calculate the payment of manager:
    def pay(self,wage,pf_percent):
        basic=wage*30
        hra=basic*0.8
        da=basic*0.2
        pf=basic*(0.01*pf_percent)
        netsalary=basic+hra+da-pf
        print(netsalary)
        return netsalary
    
class Executive(Employee):
    def __init__(self,eid,name,post,pf_percent,netsal):
        super().__init__(eid,name)
        self.post=post
        self.pf_percent=pf_percent
        self.netsal=netsal
    def __str__(self):
        pass
#calculate the payment of executive:
    def pay(self,wage,pf_percent):
        basic=wage*30
        hra=basic*0.8
        da=basic*0.2
        pf=basic*(0.01*pf_percent)
        netsalary=basic+hra+da-pf
        print(netsalary)
        return netsalary
        




#############################
####Using pandas for sorting a file ######
import pandas as pd
import numpy as np
order_df=pd.DataFrame()
order_df['customer_id']=[1,2,3,4,5,6,7]
order_df['customer_name']=['tanmay','prerana','sakshi','roy','tanu','rahul','mansur ']
order_df['Items']=['lollypop','marshmello','nougat','oreo','pie','cherry','mango']
print(order_df)


#####hence we use slicing:
count_number_of_order= lambda x:len(x.unique())
order_df['count_number_of_order']=(order_df.groupby(['customer_id'])['customer_name'].transform(count_number_of_order))
###apply the function to each group seperately####
print(order_df)

############now use lambda seperately
count_the_price=lambda y:len(y.unique())
order_df['count_the_price']=(order_df.groupby(['count_number_of_order','customer_name'])['Items'].transform(count_the_price))
print(order_df)







#########################
#######quiz######
from tkinter import Tk, Frame, Label, Button 
from time import sleep

class Question:
    def __init__(self, question, answers, correctLetter):
        self.question = question
        self.answers = answers
        self.correctLetter = correctLetter

    def check(self, letter, view):
        global right
        if(letter == self.correctLetter):
            label = Label(view, text="you're Right!")
            right += 1
        else:
            label = Label(view, text="you're Wrong!")
        label.pack()
        view.after(1000, lambda *args: self.unpackView(view))


    def getView(self, window):
        view = Frame(window)
        Label(view, text=self.question).pack()
        Button(view, text=self.answers[0], command=lambda *args: self.check("A", view)).pack()
        Button(view, text=self.answers[1], command=lambda *args: self.check("B", view)).pack()
        Button(view, text=self.answers[2], command=lambda *args: self.check("C", view)).pack()
        Button(view, text=self.answers[3], command=lambda *args: self.check("D", view)).pack()
        return view

    def unpackView(self, view):
        view.pack_forget()
        askQuestion()

def askQuestion():
    global questions, window, index, button, right, number_of_questions 
    if(len(questions) == index + 1):
        Label(window, text="Thank you've answered it. " + str(right) + " right questions out of " + str(number_of_questions) + " questions").pack()
        return
    button.pack_forget()
    index += 1
    questions[index].getView(window).pack()

questions = []
file = open("quiz.txt", "r")#######you can make any question and answer file here with proper 
line = file.readline()
while(line != ""):
    questionString = line
    answers = []
    for i in range (4):
        answers.append(file.readline())

    correctLetter = file.readline()
    correctLetter = correctLetter[:-1]
    questions.append(Question(questionString, answers, correctLetter))
    line = file.readline()
file.close()
index = -1
right = 0
number_of_questions = len(questions)
window = Tk()
button = Button(window, text="Start", command=askQuestion)
button.pack()
window.mainloop()


########################